package cl.fernando;

import java.util.ArrayList;
/*
*** author FernandoSierraProfe
 */
public class Listas {

        //01 crear clase Main
        public static void main(String[] args) {
            //02 creacion arrayList e importar
            ArrayList<String> cadenas = new ArrayList<String>();


            //03 agregar elementos
            cadenas.add("Hola");
            cadenas.add("como");
            cadenas.add("estas");
            //04 tamaño de mi arrayList
            System.out.println(cadenas.size());
            //05 visualizar mi ArrayList
            System.out.println(cadenas);

            //06 recorrer Array con for
            for (int i = 0; i < cadenas.size(); i++) {
                //6.1 con el metodo get obtengo un elemento en particular
                System.out.println("-->" + cadenas.get(i));
            }
            //07 eliminar elementos del array
            //7.1 recordar que el remove puede ser mediante el numero del elemento o el valor del elemento
            cadenas.remove(1);
            //08 recorrer nuevamente el ArrayList con el elemento ya eliminado
            for (int i = 0; i < cadenas.size(); i++) {
                System.out.println("-->" + cadenas.get(i));
            }
            //09 eliminar contenido del ArrayList
            cadenas.clear();
            System.out.println(cadenas);

            //10 agregar elementos a partir de una validacion si el Array esta vacio
            //10.1 si quiere que muestre la info del else, favor comentar la linea 33
            if (cadenas.isEmpty()){
                cadenas.add("Soy un valor cualquiera");
            }else{
                System.out.println("la cadena no esta vacia");
            }
            System.out.println(cadenas);

            //11 podriamos crear un arreglo de enteros e ir probando las propiedades ...


        }
}
